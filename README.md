# Kotlin, Spring and MongoDB test project #

Simple webapplication written in Kotlin. Using Spring Boot and MongoDB

### What is it ###

Learning the amazing Kotlin language and seeing how it fits with the Spring framework

### Contact ###
thomas.andersen77@gmail.com