package org.andtho.kotlin.web.restkotlin

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import java.time.LocalDate

internal class PersonTest {
    @org.junit.jupiter.api.Test
    fun test_alder() {
        val thomas = Person(1, "", "", LocalDate.of(1977, 9, 7))
        assertEquals(40, thomas.alder())
    }

    @org.junit.jupiter.api.Test
    internal fun test_constructor() {
        val person = Person()
        assertNotNull(person)
    }

}