
package org.andtho.kotlin.web.restkotlin

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.*
import org.springframework.data.mongodb.core.query.Query
import org.springframework.test.context.junit4.SpringRunner
import java.util.function.Consumer
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RestKotlinApplicationTests {

	@Autowired lateinit var restTemplate: TestRestTemplate
	@Autowired lateinit var mongoTemplate: MongoTemplate


	@Test
	fun getPersonById() {
		assertNotNull(restTemplate)
		val responseEntity = restTemplate.getForEntity("/person/0", Person::class.java)
		assertNotNull(responseEntity)
		assertEquals(200, responseEntity.statusCodeValue)
	}

	@Test
	fun createPerson() {
		val name = "thomas"
		val personUnderTest = Person(firstname = name, lastname = "andersen")
		val responseEntity = restTemplate.postForEntity("/person", personUnderTest, Person::class.java)
		assertNotNull(responseEntity)
		assertEquals(200, responseEntity.statusCodeValue)

		val query = Query()
				.addCriteria(
				where("firstname").`is`(name))
		val users = mongoTemplate.find(query, Person::class.java)

		users.forEach(Consumer { System.err.println(it.firstname + " " + it.lastname) })

		assertEquals(name,  users.first().firstname)

	}
}
