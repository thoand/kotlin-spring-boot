package org.andtho.kotlin.web.restkotlin

import java.time.LocalDate
import java.time.Period

data class Person(var id : Int = 0, val firstname : String, val lastname : String, val birthdate : LocalDate = LocalDate.now()) {
    constructor() : this(0, "", "", LocalDate.now())

    fun alder() : Int = Period.between(birthdate, LocalDate.now()).years


}