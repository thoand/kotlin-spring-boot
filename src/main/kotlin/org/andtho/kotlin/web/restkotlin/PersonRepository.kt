package org.andtho.kotlin.web.restkotlin

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class PersonRepository @Autowired constructor(val template: MongoTemplate) {

    fun getPersonById(id: Int) : MutableList<Person>? {
        Objects.requireNonNull(template, "MongoTemplate can not be null")
        val query = Query()
        query.addCriteria(Criteria.where("firstname").`is`("thomas"))
        return template.find(query, Person::class.java)
    }

    fun createPerson(person: Person) : Person {
        template.insert(person)
        return person
    }

}