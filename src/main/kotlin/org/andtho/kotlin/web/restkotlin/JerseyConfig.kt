package org.andtho.kotlin.web.restkotlin

import com.fasterxml.jackson.databind.ObjectMapper
import org.glassfish.jersey.server.ResourceConfig
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component
import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider

@Component
final class JerseyConfig : ResourceConfig() {

    init {
        registerEndpoints()
    }

    fun registerEndpoints() {
        register(PersonResource::class.java)
    }
}

@Component
@Provider
class MyObjectMapper : ContextResolver<ObjectMapper> {
    override fun getContext(p0: Class<*>?): ObjectMapper {
        return ObjectMapper().findAndRegisterModules()
    }

}